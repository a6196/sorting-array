import { Component } from '@angular/core';
import {SortPipe} from "../core/pipes/sort.pipe";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'sort-tuto';

  myArr = [
    { name: "G", sex: "F", age: 15 , id: 0, valid: true},
    { name: "B", sex: "M", age: 25 , id: 1, valid: false},
    { name: "Z", sex: "F", age: 18 , id: 2, valid: false},
    { name: "A", sex: "F", age: 12 , id: 3, valid: true},
    { name: "H", sex: "M", age: 19 , id: 4, valid: false}
  ];
  index=0;
  newValue = '';

  constructor(private sortPipe: SortPipe) {}

  addElement(){
    const indx = this.myArr.length;
    this.myArr.push({ name: "A", sex: "F", age: 24, id: indx, valid: indx % 2 === 0});
  }

  editElement(){
    this.myArr[this.index].valid = this.newValue === 'true';
  }

  getArrays(){
    let sortedByName = this.sortPipe.transform(this.myArr, "asc", "name");
    return this.sortPipe.transform(sortedByName, "desc", "valid");
  }
}
